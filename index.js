const BASE_URL = "https://62db6ca2e56f6d82a77284cd.mockapi.io";


function getDSSV() {
    batLoading();
    axios({
    url:`${BASE_URL}/sv`,
    method:"GET",
})
.then(function(res){
    let newDSSV = res.data.map(function(item){
        let sv = new newSV(item.id,item.name,item.email,item.matKhau,item.math,item.physics,item.chemistry);
        return sv;
    });
    renderDSSV(newDSSV);

    tatLoading();
})
.catch(function(err){
    tatLoading();
    console.log(err);
})};

getDSSV();

function xoaSinhVien(id){
    if(document.getElementById("updateDataBtn").disabled == false){
        resetInputVal();
        deactiveUpdateBtn();
    }
    batLoadingXoaItem(id);
    axios({
        url:`${BASE_URL}/sv/${id}`,
        method: "DELETE",
    })
    .then(function(res){
        getDSSV();
    }).catch(function(err){

    });
}

function themSV(){
    let newSV = layThongTinTuForm();
    console.log(newSV);

    batLoading();
    axios({
        url:`${BASE_URL}/sv`,
        method:"POST",
        data : newSV,
    })
    .then(function(res){
        getDSSV();
    })
    .catch(function(err){
        tatLoading();
        console.log(err);
    })
}

function editSV(id){
    if(document.getElementById("updateDataBtn").disabled == false){
        enableAllDeleteBtn();
    }
    batLoadingCapNhat(id);
    disableDeleteBtn(id);
    axios({
        url:`${BASE_URL}/sv/${id}`,
        method:"GET",
    }).then(function(res){
        hienThongTinChinhSua(res.data);
        tatLoadingCapNhat(id);
        activeUpdateBtn();
        deactiveAddSVBtn();
    }).catch(function(err){
        tatLoadingCapNhat(id);
        enableAllDeleteBtn();
        console.log(err);
    })
}


function capNhatThongTinSV(){
    let newSV = layThongTinTuForm();
    let id = newSV.id;
    batLoadingCapNhatItem();
    axios({
        url:`${BASE_URL}/sv/${id}`,
        method:"PUT",
        data:newSV,
    }).then(
        (res) =>{
            getDSSV();
            deactiveUpdateBtn();
            clearInputForm();
            activeAddSVBtn();
            tatLoadingCapNhatItem();
        }
    ).catch((err)=>{
        console.log(err);
        deactiveUpdateBtn();
        activeAddSVBtn();
        tatLoadingCapNhatItem();
    })
}

function searchName(){
    let searchKey = document.getElementById("txtSearch").value;
    if(searchKey!=""){
        batLoadingSearch();
        axios({
            url:`${BASE_URL}/sv`,
            method: "GET",
        }
        ).then((res)=>{
            let sv = res.data;
            let newDSSV = sv.map((item)=>{
                return new newSV(item.id,item.name,item.email,item.matKhau,item.math,item.physics,item.chemistry);
            });
            var foundItem = newDSSV.filter((item)=>{
                return item.name == searchKey;
            });
            if(foundItem == undefined){
              alert("Không tìm thấy " + searchKey);  
            } else {
                renderDSSV(foundItem);
            }
            tatLoadingSearch();
        }
        ).catch((err)=>{
            console.log(err);
            tatLoadingSearch();
        }
        );
    }
}


