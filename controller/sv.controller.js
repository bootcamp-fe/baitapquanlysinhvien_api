function layThongTinTuForm() {
    const maSv = document.getElementById("txtMaSV").value;
    const tenSv = document.getElementById("txtTenSV").value;
    const email = document.getElementById("txtEmail").value;
    const matKhau = document.getElementById("txtPass").value;
    const diemToan = document.getElementById("txtDiemToan").value;
    const diemLy = document.getElementById("txtDiemLy").value;
    const diemHoa = document.getElementById("txtDiemHoa").value;
  
    return new newSV(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
  }

renderDSSV = function(dssv){
    var contentHTML = "";
    for (var i = 0 ; i < dssv.length; i++){
        var sv = dssv[i];
        var contentTr =`<tr>
        <td>${sv.id}</td>
        <td>${sv.name}</td>
        <td>${sv.email}</td>
        <td>${sv.tinhDTB()}</td>
        <td>
        <button onclick = xoaSinhVien('${sv.id}') class="btn btn-warning"><i class="fa-solid fa-trash-can"></i></button>
        <button onclick = editSV('${sv.id}') class="btn btn-primary"><i class="fa-regular fa-pen-to-square"></i></button></td>
        </tr>`;
        contentHTML += contentTr;
    };
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}

function hienThongTinChinhSua(sv){
    document.getElementById("txtMaSV").value=sv.id;
    document.getElementById("txtTenSV").value=sv.name;
    document.getElementById("txtEmail").value=sv.email;
    // document.getElementById("txtPass").value=sv.matKhau;
    document.getElementById("txtDiemToan").value=sv.math;
    document.getElementById("txtDiemLy").value=sv.physics;
    document.getElementById("txtDiemHoa").value=sv.chemistry;
  }