function batLoading(){
    document.querySelector('.tbody-cover').style.display = "block";
    document.getElementById("loading").style.display = "block";
}

function tatLoading(){
    document.getElementById("loading").style.display = "none";
    document.querySelector('.tbody-cover').style.display = "none";
}

function batLoadingCapNhat(id){
    let index = -1;
    let tbody = document.querySelectorAll('#tbodySinhVien tr > td:first-child');
    for (i = 0 ; i < tbody.length ; i++){
        if(tbody[i].innerText == id){
            index = i;
            break;
        }
    }
    if(index != -1){
        let edittingTr = document.querySelectorAll('#tbodySinhVien tr')[index];
        let edittingBtn = edittingTr.querySelector('td button.btn-primary');
        edittingBtn.innerHTML = `<span class="spinner-border spinner-border-sm"></span>`;
    }
}


function tatLoadingCapNhat(id){
    let index = -1;
    let tbody = document.querySelectorAll('#tbodySinhVien tr > td:first-child');
    for (i = 0 ; i < tbody.length ; i++){
        if(tbody[i].innerText == id){
            index = i;
            break;
        }
    }
    if(index != -1){
        let edittingTr = document.querySelectorAll('#tbodySinhVien tr')[index];
        let edittingBtn = edittingTr.querySelector('td button.btn-primary');
        edittingBtn.innerHTML = `<i class="fa-regular fa-pen-to-square"></i>`;
    }
}

function batLoadingXoaItem(id){
    let index = -1;
    let tbody = document.querySelectorAll('#tbodySinhVien tr > td:first-child');
    for (i = 0 ; i < tbody.length ; i++){
        if(tbody[i].innerText == id){
            index = i;
            break;
        }
    }
    if(index != -1){
        let edittingTr = document.querySelectorAll('#tbodySinhVien tr')[index];
        let edittingBtn = edittingTr.querySelector('td button.btn-warning');
        edittingBtn.innerHTML = `<span class="spinner-border spinner-border-sm"></span>`;
    }
}

function batLoadingCapNhatItem(){
    var updateBtn = document.getElementById("updateDataBtn");
    updateBtn.innerHTML = `<span class="spinner-border spinner-border-sm"></span>`;
}



function tatLoadingCapNhatItem(id){
    var updateBtn = document.getElementById("updateDataBtn");
    updateBtn.innerHTML = `Cập Nhật`;
}

function batLoadingSearch(){
    var updateBtn = document.getElementById("btnSearch");
    updateBtn.innerHTML = `<span class="spinner-border spinner-border-sm"></span>`;
}

function tatLoadingSearch(){
    var updateBtn = document.getElementById("btnSearch");
    updateBtn.innerHTML = `Search`;
}


function disableDeleteBtn(id){
    let index = -1;
    let tbody = document.querySelectorAll('#tbodySinhVien tr > td:first-child');
    for (i = 0 ; i < tbody.length ; i++){
        if(tbody[i].innerText == id){
            index = i;
            break;
        }
    }
    if(index != -1){
        let edittingTr = document.querySelectorAll('#tbodySinhVien tr')[index];
        let edittingBtn = edittingTr.querySelector('td button.btn-warning');
        edittingBtn.disabled = true;
    }
}

function enableAllDeleteBtn(){
    let deleteBtn = document.querySelectorAll('td button.btn-warning');
    for (item of deleteBtn){
        item.disabled = false;
    }
}

function activeUpdateBtn(){
    document.getElementById("updateDataBtn").disabled = false;
}

function deactiveUpdateBtn(){
    document.getElementById("updateDataBtn").disabled = true;
}

function activeAddSVBtn(){
    document.getElementById("themSVBtn").disabled = false;
}

function deactiveAddSVBtn(){
    document.getElementById("themSVBtn").disabled = true;
}

function clearInputForm(){
    inputItem = document.querySelectorAll('#formQLSV input');
    inputItem.forEach(element => {
        element.value = "";
    });
}

function resetInputVal(){
    clearInputForm();
    activeAddSVBtn();
    enableAllDeleteBtn();
    getDSSV();
}

